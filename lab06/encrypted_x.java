import java.util.Scanner;

public class encrypted_x{
  
  public static void main(String [] args){
    
    Scanner keyboard = new Scanner (System.in);
    
    // declaring, initializing variables
    Boolean correctVal = true;
    int dimensions = -1, n = 0;
    
    // soliciting user input
    System.out.print("This program will create an X-pattern with asteriks and spaces according to your specified dimensions.");
    System.out.println();
    System.out.print("Input an integer between 0-100: ");
    
    // validating user input -- did user input integer?
    if (keyboard.hasNextInt()){
      dimensions = keyboard.nextInt();
    }
    else {
      keyboard.nextLine(); 
    }
    
    // validating user input -- is integer between 0 and 100?
    if (dimensions > 100){
      correctVal = false;
    }
    else if (dimensions < 0){
      correctVal = false;
    }
    
    while (!correctVal){
      System.out.print("Input an integer between 0-100: ");
        if (keyboard.hasNextInt()){
          dimensions = keyboard.nextInt();
          correctVal = true;
        }
        else {
          keyboard.nextLine();
        }
        if (dimensions > 100){
          correctVal = false;
        }
        else if (dimensions < 0){
          correctVal = false;
        }
    }
    
    
    if ((dimensions % 2) == 1){
      for (int i = 0; i < (dimensions); i++){
        for (int j = 0; j < (dimensions); j++){
          if (i < (dimensions/2)) {
            if (i==j) {
              System.out.print(" ");
            }
            if ((dimensions-i)==(j+1)) {
              System.out.print(" ");
            }
            else{
              System.out.print("*");
            }
          }
          if (i == (dimensions/2)){
            if (i==j){
              System.out.print(" ");
            }
            if ((dimensions-i)==(j+1)) {
              System.out.print(" ");
            }
            else {
              System.out.print("*");
            }
           }
          if (i > (dimensions/2) && (i < dimensions)){
            if (i==(j-1)){
              System.out.print(" ");
            }
            if ((dimensions-(i)==(j+1))) {
              System.out.print(" ");
            }
            else {
              System.out.print("*");
            }
          }
        }
        System.out.println();
      }
    }
    if ((dimensions % 2) == 0){
      for (int i = 0; i < (dimensions); i++){
        for (int j = 0; j < (dimensions); j++){
          if (i < (dimensions/2)) {
            if (i==j) {
              System.out.print(" ");
            }
            if ((dimensions-i)==(j+1)) {
              System.out.print(" ");
            }
            else{
              System.out.print("*");
            }
          }
          if (i == (dimensions / 2)){
            if ((i+1)==j){
              System.out.print(" ");
            }
            if ((dimensions-j)==(i+1)) {
              System.out.print(" ");
            }
            else {
              System.out.print("*");
            }
          }
          if (i == ((dimensions / 2))+(1)){ 
          }
          if (i > (dimensions/2) && (i < dimensions)){
            if (i==(j-1)){
              System.out.print(" ");
            }
            if ((dimensions-(i)==(j+1))) {
              System.out.print(" ");
            }
            else {
              System.out.print("*");
            }
          }
        }
        System.out.println();
      }
    }
  }
}