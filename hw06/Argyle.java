// Will Newbegin
// CSE 002
// Prof. Chen
// March 16 2018

// This program will output an argyle pattern according to height/width specicfications provided
// through user input. After user input, the program will evaluate whether or not the provided
// input is valid. If it is not, it will ask the user for input again. Following that, the
// program will run the proper input through the code and print the desired argyle pattern.


// importing scanner for user input
import java.util.Scanner;

// providing for the program's basic needs
public class Argyle {
  
  public static void main (String [] args){
    
    // declaring scanner
    Scanner keyboard = new Scanner (System.in);
    
    // declaring, initialzing variables
    int windowHeight = 0, windowWidth= 0, diamondWidth = 0, stripeThickness = 0;
    String diamondPattern1 = "Get out of my shoes", diamondPattern2 = "whoops?", stripePattern = "fight milk";
    Boolean correctInt = true;
    
    // soliciting user input, window height
    System.out.print("Enter a positive integer for the height of the viewing window: ");
    
    //validating window height input 
    if (keyboard.hasNextInt()){
      windowHeight = keyboard.nextInt();
    }
    else {
      keyboard.nextLine();
    }
    if (windowHeight < 1){
      correctInt = false;
    }
    
    // re-evaluating window height input if not valid
    while (!correctInt){
      System.out.print("Enter a positive integer for the height of the viewing window: ");
      if (keyboard.hasNextInt()){
        windowHeight = keyboard.nextInt();
        correctInt = true;
      }
      else {
        keyboard.nextLine();
      }
      if (windowHeight >= 1){
        correctInt = true;
      }
      else {
        correctInt = false;
      }
    }
    
    
    // soliciting user input, window width
    System.out.print("Enter a positive integer for the width of the viewing window: ");
    
    //validating window width input 
    if (keyboard.hasNextInt()){
      windowWidth = keyboard.nextInt();
    }
    else {
      keyboard.nextLine();
    }
    if (windowWidth < 1){
      correctInt = false;
    }
    
    // re-evaluating window width input if not valid
    while (!correctInt){
      System.out.print("Enter a positive integer for the width of the viewing window: ");
      if (keyboard.hasNextInt()){
        windowWidth = keyboard.nextInt();
        correctInt = true;
      }
      else {
        keyboard.nextLine();
      }
      if (windowWidth >= 1){
        correctInt = true;
      }
      else {
        correctInt = false;
      }
    }
    
    // soliciting user input, diamond width
    System.out.print("Enter an even positive integer for the width of the argyle diamonds: ");
    
    //validating diamond width input 
    if (keyboard.hasNextInt()){
      diamondWidth = (keyboard.nextInt());
    }
    else {
      keyboard.nextLine();
    }
    if (diamondWidth < 1){
      correctInt = false;
    }
    
    // re-evaluating diamond width input if not valid
    while (!correctInt){
      System.out.print("Enter an even positive integer for the width of the viewing window: ");
      if (keyboard.hasNextInt()){
        diamondWidth = keyboard.nextInt();
        correctInt = true;
      }
      else {
        keyboard.nextLine();
      }
      if (diamondWidth >= 1){
        correctInt = true;
      }
      else {
        correctInt = false;
      }
    }
    
    
    // soliciting user input, stripe thickness
    System.out.print("Enter an odd positive integer for the thickness of the argyle's main stripe: ");
    
    //validating stripe thickness input 
    if (keyboard.hasNextInt()){
      stripeThickness = (keyboard.nextInt());
    }
    else {
      keyboard.nextLine();
    }
    if (stripeThickness < 1){
      correctInt = false;
    }
    else if ((stripeThickness % 2 == 0)) {
      correctInt = false;
    }
    
    // re-evaluating stripe thickness input if not valid
    while (!correctInt){
      System.out.print("Enter an odd positive integer for the thickness of the argyle's main stripe: ");
      if (keyboard.hasNextInt()){
        stripeThickness = keyboard.nextInt();
        correctInt = true;
      }
      else {
        keyboard.nextLine();
      }
      if (stripeThickness >= 1){
        correctInt = true;
      }
      else {
        correctInt = false;
      }
      if (stripeThickness % 2 == 1){
        correctInt = true;
      }
      if (stripeThickness % 2 == 0){
        correctInt = false;
      }
    }
    
    // soliciting user input, first pattern fill
    System.out.print("Enter a character for the first pattern fill: ");
    
    // validating fill pattern 1 input
    if (keyboard.hasNext()){
      diamondPattern1 = keyboard.next();
      char result = diamondPattern1.charAt(0);
    }
    else {
      keyboard.nextLine();
    }
    
    
    // soliciting user input, second pattern fill
    System.out.print("Enter a character for the second pattern fill: ");
    
    // validating fill pattern 2 input
    if (keyboard.hasNext()){
      diamondPattern2 = keyboard.next();
      char result = diamondPattern2.charAt(0);
    }
    else {
      keyboard.nextLine();
    }
    
    
    // soliciting user input, stripe pattern fill
    System.out.print("Enter a character for the second pattern fill: ");
    
    // validating stripe fill pattern input
    if (keyboard.hasNext()){
      stripePattern = keyboard.next();
      char result = stripePattern.charAt(0);
    }
    else {
      keyboard.nextLine();
    }
    
    // begin print statement, calculations
    // (diamondWidth -(j % diamondWidth))
    for (int i = 0; i < windowHeight; i++){
      for (int j = 0; j < windowWidth; j++){
        if (i % (2*diamondWidth) < diamondWidth){ //this will cover top left and top right "quarters" of the pattern
          if (j % (2 * diamondWidth) < diamondWidth){ // this will detect where we are in the pattern row
            if((((i % diamondWidth)-( j % diamondWidth)) <= ((int)stripeThickness/2)) && (i%diamondWidth)-(j%diamondWidth) >= -1 *(int)(stripeThickness/2)){
              System.out.print(stripePattern);
            }
            else if ((diamondWidth-(j%diamondWidth)) <=i){
              System.out.print(diamondPattern2);
            }
            else {
              System.out.print(diamondPattern1);
            }
          }
          else {
            if((((i % diamondWidth)-(diamondWidth -(j % diamondWidth))) <= ((int)stripeThickness/2)) && (i % diamondWidth)-(diamondWidth -(j % diamondWidth)) >= -1 *(int)(stripeThickness/2)){
              System.out.print(stripePattern);
            }
            else if ((diamondWidth-(diamondWidth -(j % diamondWidth))) <=i){
              System.out.print(diamondPattern2);
            }
            else {
              System.out.print(diamondPattern1);
            }
          }
        }
        else {
          
        }  
      }
    } 
  } 
}