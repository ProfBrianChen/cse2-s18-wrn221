 // Will Newbegin


import java.util.Scanner;

public class Hw06{
  
  public static void main (String [] args){
    
    // identifying scanner w/ variable name "S"
    Scanner S = new Scanner (System.in);
    
    // declaring/initializing variables outside specific scopes
    String nameTeacherLast = "ouch", nameTeacherTitle = "ouch", nameDpt = "ouch", classTimeAMPM = "ouch", numCourseString = "ouch";
    int numCourse = 0, numMeetsPerWeek = 0, numStudents = 0 , classStartTimeHour = 0, classStartTimeMinutes = 0;
     
    
    
    //obtaining department name
    System.out.print("Enter the name of the department: ");
      if (S.hasNext() == true){
        nameDpt = S.next();
        
    }
    // checking for proper input -- this will occur for each round of user input  
    else {
        while (S.hasNext() == false){
          System.out.print("Please enter the department's name!: ");
        }
      } 
    
    
    //obtaining course number
    System.out.print("Enter the course number (not the department!): ");
      if (S.hasNextInt() == true){
          numCourse = S.nextInt();
        // creating proper syntax for course number output
        if (numCourse <10){
            numCourseString = String.valueOf(numCourse);
            numCourseString = "00" + numCourseString;
        }
        if ((numCourse < 100) && (numCourse >= 10)){
          numCourseString = String.valueOf(numCourse);
          numCourseString = "0" + numCourseString;
        }
      }
      else {
        while (S.hasNextInt() == false){
          System.out.print("Please enter the course number (not the department!) in INTEGER form: ");
        }
      
     }
    
    
    // obtaining number of times course happens per week
    System.out.print("Enter the number of times the course occurs per week: ");
      if (S.hasNextInt() == true){
        numMeetsPerWeek = S.nextInt();
      
    }
      else {
        while (S.hasNextInt() == false){
          System.out.print("Enter the number of times the course occurs per week: ");
        }
      } 
    
    
    //obtaining Professor's last name
    System.out.print("Enter the last name of the Professor: ");
      if (S.hasNext() == true){
        nameTeacherLast = S.next();
      
    }
      else {
        while (S.hasNext() == false){
          System.out.print("Please enter the Professor's last name: ");
        }
      } 
    
    
    //obtaining Professor's title
    System.out.print("Enter the Professor's title (Dr., Prof., etc.): ");
      if (S.hasNext() == true){
        nameTeacherTitle = S.next();

      }
      else {
        while (S.hasNext() == false){
          System.out.print("Please enter the Professor's title: ");
        }
      }
    
    
    //obtaining class start times, hour
    System.out.print("Enter the hour of the class's start time: ");
      if (S.hasNextInt() == true){
        classStartTimeHour = S.nextInt();
      
    }
      else {
        while (S.hasNextInt() == false){
          System.out.print("Enter the hour of the class's start time: ");
        }
      }
    
    
    // obtaining class start time, minutes
    System.out.print("Enter the minute of the class's start time: ");
      if (S.hasNextInt() == true){
        classStartTimeMinutes = S.nextInt();
      
    }
      else {
        while (S.hasNextInt() == false){
          System.out.print("Enter the minute of the class's start time: ");
        }
      }
    
    
    // obtaining class start times, AM or PM
    System.out.print("Does the class take place in the AM or PM?: ");
      if (S.hasNext() == true){
        classTimeAMPM = S.next();

      }
      else {
        while (S.hasNext() == false){
          System.out.print("Does the class take place in the AM or PM?: ");
        }
      }
    
  // outputting results
   System.out.print("You are taking " + nameDpt + " " + numCourseString); 
   System.out.print(" with " + nameTeacherTitle + " " + nameTeacherLast + ". You have the class " + numMeetsPerWeek);
   System.out.print(" times a week at " + classStartTimeHour + ":" + classStartTimeMinutes + " " + classTimeAMPM + ".");
  }
    
    
}