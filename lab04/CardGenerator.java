// Will Newbegin
// CSE 002
// Prof. Chen
// This program will simulate the random picking of a card
// from a standard deck, sans Jokers. Cards 1-13 will 
// represent the diamonds, with the ace of diamonds as 1, 
// and the king as 13. In essence, the cards correspond 
// with their respective value in the deck. This follows 
// for clubs (14-26), hearts (27-39), and spades (40-52). 



public class CardGenerator{ 
    
    public static void main(String [] args){
   
   // creating random integer, from 1-52,
   // to designate as a standard card   
   int CardNum = (int) (Math.random() * 52 +1);
   // base initialization, declaration 
   String CardValue = "blank";
   String CardSuit = (String) "what's up dad?";
   // Creating a system to determine the value of
   // the card in easier fashion via switch variable    
   int CardAssign = CardNum % 13;
   
    // determining the suit of the card
    if (CardNum <= 13){
      CardSuit = "diamonds";
    }
    if (CardNum >= 14 && CardNum < 27){
      CardSuit = "clubs";
    }
    if (CardNum >= 27 && CardNum < 40){
      CardSuit = "hearts"; 
    }
    if (CardNum >= 40){
      CardSuit = "spades";
    }
     
     // designating which numbers (w/ help of modulus fxn.) are assigned to each card value
     switch (CardAssign) {
       case 1: 
         CardValue = (String)  "ace";
         break;
       case 2:
         CardValue = (String) "two";
         break;
       case 3: 
         CardValue = (String) "three";
         break;
       case 4: 
         CardValue = (String) "four";
         break; 
       case 5: 
         CardValue = (String) "five";
         break;
       case 6: 
         CardValue = (String) "six";
         break;
       case 7: 
         CardValue = (String) "seven";
         break;
       case 8: 
         CardValue = (String) "eight";
         break;
       case 9: 
         CardValue = (String) "nine"; 
         break;
       case 10: 
         CardValue = (String) "ten";
         break;
       case 11: 
         CardValue = (String) "Jack";
         break;
       case 12: 
         CardValue = (String) "Queen";
         break;
       case 13: 
         CardValue = (String) "King";
         break; 
     }
     
     
      // printing results
      System.out.println("You have drawn the " + CardValue + " of " + CardSuit + ".");
      
    } 
     
  
  } 