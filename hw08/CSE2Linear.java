// Will Newbegin
// CSE2
// Prof. Chen
// This program will allow user to input a set of grades in any order, search through them linearly
// scramble them, and then search through them binarily.


import java.util.Scanner; // importing our Scanner buddy
import java.util.Random; //importing our Random buddy
import java.util.*; // importing Array methods to assist in programming

public class CSE2Linear{
  
  public static void main(String[] args){
    
    // declaring, initializing Scanner and Randomizer
    Scanner keyboard = new Scanner (System.in);
    
    // variables
    int [] studentGrades;
    studentGrades = new int[15]; 
    int gradeSearch = 0;
    Boolean intCheck = true;
    
    for(int i = 0; i<15; i++){ // intializing studentGrades
      studentGrades[i]=i;
    }
    
    System.out.println("Please enter 15 grades (as integers) ranging from 1-100.");
    for(int j = 0; j<15; j++){ // soliciting user input
      System.out.print("Enter grade #" + (j+1) + ": ");
      if (keyboard.hasNextInt()){ //seeing if input is type int
        studentGrades[j] = keyboard.nextInt();
      }
      else {
        keyboard.nextLine();
      }
      if (studentGrades[j]<1){
          intCheck=false;
      }
      else if (studentGrades[j]>100){
        intCheck=false;
      }
      else{
        intCheck=true;
      }
      
      while(!intCheck){ // checking for proper integer input
        System.out.print("Please enter grade #" + (j+1) + " (as an integer) ranging from 1-100: ");
        if (keyboard.hasNextInt()){ //seeing if input is type int
          studentGrades[j] = keyboard.nextInt();
        }
        else {
          keyboard.nextLine();
          intCheck = false;
        }
        if (studentGrades[j]<1){
          intCheck=false;
        }
        else if (studentGrades[j]>100){
          intCheck=false;
        }
        else{
          intCheck=true;
        }
      }
    } 
    
    //sorting, printing array in order
    Arrays.sort(studentGrades);
    System.out.println("From high to low, the grades are: ");
    System.out.println(Arrays.toString(studentGrades));
    
    
    
    // asking which grade to search for, linearly
    System.out.print("Enter the grade (once again, an integer between 1-100) to search for, linearly: ");
    
    if (keyboard.hasNextInt()){ //seeing if input is type int
        gradeSearch = keyboard.nextInt();
    }
    else {
      keyboard.nextLine();
    }
    if (gradeSearch<1){
        intCheck=false;
    }
    else if (gradeSearch>100){
      intCheck=false;
    }
    else{
      intCheck=true;
    }
    while(!intCheck){ // checking for proper integer input
      System.out.print("Please enter grade # (as an integer) ranging from 1-100: ");
      if (keyboard.hasNextInt()){ //seeing if input is type int
        gradeSearch = keyboard.nextInt();
      }
      else {
        keyboard.nextLine();
        intCheck = false;
      }
      if (gradeSearch<1){
        intCheck=false;
      }
      else if (gradeSearch>100){
        intCheck=false;
      }
      else{
        intCheck=true;
      }
    }
  
  linear(studentGrades, gradeSearch); // from here we call our linear search through linear
  System.out.println("Your code will now scramble.");
  
  scrambling(studentGrades); // calling array scramble
    
  // binary search call
  System.out.print("Enter the grade (once again, an integer between 1-100) to search for, binarily: ");
    
    if (keyboard.hasNextInt()){ //seeing if input is type int
        gradeSearch = keyboard.nextInt();
    }
    else {
      keyboard.nextLine();
    }
    if (gradeSearch<1){
        intCheck=false;
    }
    else if (gradeSearch>100){
      intCheck=false;
    }
    else{
      intCheck=true;
    }
    while(!intCheck){ // checking for proper integer input
      System.out.print("Please enter grade # (as an integer) ranging from 1-100: ");
      if (keyboard.hasNextInt()){ //seeing if input is type int
        gradeSearch = keyboard.nextInt();
      }
      else {
        keyboard.nextLine();
        intCheck = false;
      }
      if (gradeSearch<1){
        intCheck=false;
      }
      else if (gradeSearch>100){
        intCheck=false;
      }
      else{
        intCheck=true;
      }
    }
  
  binary(studentGrades, gradeSearch);
  
    
  }
  
  public static void linear(int studentGrades[], int gradeSearch){ // linear search method
    boolean linCheck = false;
    for (int i = 1; i < 15; i++){
      if (studentGrades[i] == gradeSearch){
        System.out.println(gradeSearch + " was found in the list.");
        linCheck = true;
      }
    }
    if (!linCheck){
      System.out.println("Your grade was not found in this list.");
    }
    return;
  }
  
  public static void binary(int studentGrades[], int gradeSearch){ 
    int valStart = 0, valMid = 1, valEnd=0;                   
    for (int i = 0; i<studentGrades.length; i++){
      valMid = (valStart+studentGrades.length)+1;
      if (gradeSearch < studentGrades[valMid]){
        valEnd = (valMid-1);
      }
      else if (gradeSearch > studentGrades[valMid]){
        valStart = (valMid+1);
      }
      else if (gradeSearch == studentGrades[valMid]){
        System.out.println("Your grade " + gradeSearch + " was found in " + i + "iterations. ");
      }
    }
    return;
  }
  
  public static void scrambling(int studentGrades[]){ // scrambling method, using Fisher-Yates principles
    int newPos = 1, newVar =1;
    
    for (int i = 0; i < studentGrades.length; i++){
      int scrambleVal = (int) (studentGrades.length * Math.random());
      
      newVar = studentGrades[scrambleVal];
      studentGrades[scrambleVal]=studentGrades[newPos];
      studentGrades[newPos] = newVar;
    }
    System.out.println(Arrays.toString(studentGrades));
    return;
  }
}