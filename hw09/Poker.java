// Will Newbegin


public class Poker{
  
  
  public static int valueComp (int oldVal){
    int newVal = (oldVal%13) + 1;
    return newVal;
  }
  
  public static boolean suitCheck(){
    
    return true;
  }
  
  public static boolean pair(int [] hand){
    
    for(int i = 0; i<4; i++){
      for(int j = (i+1); j<5; j++){
        if(valueComp(hand[i]) == valueComp(hand[j])){
          return true;
        }
      }
    }
    return false;
  }
  
  
  public static boolean threeKind (int [] hand){
    for(int i = 0; i<3; i++){
      for(int j = (i+1); j<4; j++){
        for(int k = (j+1); k<5; k++){
          if(valueComp(hand[i]) == valueComp(hand[j]) && valueComp(hand[j]) == valueComp(hand[k])){
          return true;
          }
        }
      }
    }
    return false;
  }
  
  public static boolean fullHouse(int [] hand){
    boolean pair = false, trip = false;
    
    for(int i = 0; i<4; i++){
      for(int j = (i+1); j<5; j++){
        if(valueComp(hand[i]) == valueComp(hand[j])){
          pair = true;
        }
      }
    }
    
    for(int i = 0; i<3; i++){
      for(int j = (i+1); j<4; j++){
        for(int k = (j+1); k<5; k++){
          if(valueComp(hand[i]) == valueComp(hand[j]) && valueComp(hand[j]) == valueComp(hand[k])) {
            trip = true;
          } 
        }
      }
    }
    
    if (pair && trip){
      for(int i = 0; i<2; i++){
        for(int j = (i+1); j<3; j++){
          for(int k = (j+1); k<4; k++){
            for(int p = (k+1); p<5; p++){
              if(valueComp(hand[i]) == valueComp(hand[j]) && valueComp(hand[j]) == valueComp(hand[k]) && valueComp(hand[k]) != valueComp(hand[p])) {
                return true;
              } 
            } 
          }
        }
      }
    }
    else{
      return false;
    }
    return false;
  }
  
  
  public static void CardPrint(int [] hand, int player){
    
    for (int i =0; i<5; i++){
      int card = hand[i];
      String labelSuit = "grasp", labelType = "thrust";
      int CardAssign = card % 13;

      if (card<13){
          labelSuit="diamonds"; // cards 0-12 will be assigned "diamonds" suit
      }
      else if (card<26 && card>=13){ // cards 13-25 will be assigned "hearts" suit
        labelSuit="hearts";
      }
      else if (card<39 && card>=26){ // cards 26-38 will be assigned "spades" suit
        labelSuit="spades";
      }
      else if (card<52 && card>=39){ // cards 39-51 will be assigned "clubs" suit
        labelSuit="clubs";
      }
      switch (CardAssign){ // giving each card value, 0-51, a type
       case 0: 
         labelType = (String) "ace";
         break;
       case 1: 
         labelType = (String)  "two";
         break;
       case 2:
         labelType = (String) "three";
         break;
       case 3: 
         labelType= (String) "four";
         break;
       case 4: 
         labelType = (String) "five";
         break; 
       case 5: 
         labelType = (String) "six";
         break;
       case 6: 
         labelType= (String) "seven";
         break;
       case 7: 
         labelType = (String) "eight";
         break;
       case 8: 
         labelType = (String) "nine";
         break;
       case 9: 
         labelType = (String) "ten"; 
         break;
       case 10: 
         labelType = (String) "Jack";
         break;
       case 11: 
         labelType = (String) "Queen";
         break;
       case 12: 
         labelType = (String) "King";
         break; 
      }
      
      System.out.println("Player " + player + "'s card #" + (i+1) + " is the " + labelType + " of " + labelSuit);

    }
  }
  
  
  public static int[] shuffler(int[] cards){ // scrambling method, using Fisher-Yates principles
    int newPos = 1, newVar =1;
    
    for (int i = 0; i < 52; i++){
      int scrambleVal = (int) (52 * Math.random());
      
      newVar = cards[scrambleVal];
      cards[scrambleVal]=cards[newPos]; 
      cards[newPos] = newVar;
    }
    
    return cards;
  }
  
  public static void main (String [] args){
    int [] cards; // declaring/naming our first-level integer arrays
    int [] hand1, hand2; // declaring/naming our outer-level integer arrays
    String [] labelType, labelSuit; // declaring/naming that each 
    

    cards = new int[52]; // initialization
    hand1= new int[5];
    hand2=new int[5];
    
    
    for (int i=0; i<52;i++){ // associating card values, 0-51, with a suit and type
      int CardAssign = i % 13;
      cards[i]=i;
    }

   cards=shuffler(cards); // shuffling cards

    System.out.println("");
    System.out.println("Welcome to poker. You are player one.");
    System.out.println("The dealer will now give you and player two five cards.");
    System.out.println("Rules: if you do not have a pair, three-of-a-kind, flush, or full house, the house wins.");
    System.out.println("Otherwise, you must beat the house. If you tie, the house also wins.");
    System.out.println(" ");
    for(int i = 0; i<10; i++){
      if(i%2==0){ // this would be for player 1, who gets dealt to first, and thus has deals 0, 2, 4...8
        hand1[i/2]=cards[i]; //
        
      }
      else if(i%2==1){ // this would be for player 2, who gets dealt to second, and thus has deals 1,3, 5...9
        hand2[(int)(i/2) ]=cards[i];
         
      }
    }
    CardPrint(hand1, 1);
    CardPrint(hand2, 2);
    
    boolean win = false;
    if (pair(hand1)){
      if(pair(hand2)){
        if(threeKind(hand1)){
          if (threeKind(hand2)){
            if(fullHouse(hand1)){
              if(fullHouse(hand2)){
                win = false;
              }
              else{
                win = true;
              }
            }
            else{
              win = true;
            }
          }
          else {
            win = true;
          }
        }
        else {
          win = true;
        }
      }
      else {
        win = true;
      }
    }
    else{
      win = false;
    }
      
         
          
    
    
    System.out.println("");
    if(win){
      System.out.println("Congratulations! You have beat the dealer.");
    }
    else{
      System.out.println("Bummer! You have lost.");
    }
    System.out.println("");
    
    
    
    
    
    
    
    
    
    
    
    
    
  
    
    
    
    
   
    
  }
}