//
///CSE 2 Welcome Class Assignment
//// Will Newbegin
//// CSE 002
//// Prof. Brian Chen
////
public class WelcomeClass{
  public static void main(String [] args){
    
    System.out.println("                          -----------");
    System.out.println("                          | WELCOME |");
    System.out.println("                          -----------");
    System.out.println("                       ^  ^  ^  ^  ^  ^");
    System.out.println("                       / \\/ \\/ \\/ \\/ \\/ \\");
    System.out.println("                      <-W--R--N--2--2--1->"); 
    System.out.println("                       \\ /\\ /\\ /\\ /\\ /\\ /");                   
    System.out.println("                        v  v  v  v  v  v");
    System.out.println("My name is Will Newbegin, and I am a freshman here at LU. I'm from Allentown, PA, and my hobbies are playing guitar, writing, and listening to music. ");                   
    
        }
                       
  
}
/// This code will output a welcome "sign" displaying my Lehigh username followed by a brief bio. 