// Will Newbegin
//
// In which we will create a city w/ dimensions n x n
//0..........n
//.
//.
//.
//.
//.
//.
//.
//.
//.
//.
//

// this homework will represent a city that is 10-15 blocks by 10-15 blocks, with each block holding a population of 100-999 individuals.
// a storm of robots will invade the city, and once they do, move westward towards the capital building, which lies beyond the city
// as such, robots who reach the capital will disappear.

// I will add onto this program by making it a "game", so to speak -- if half (or more) of the robots reach the capital, humanity "loses".
// Alternatively, if less than half of the robots reach the capital, humanity has fought them off -- for now...




import java.util.Arrays;
import java.util.Random;


public class RobotAttack{
  
  public static void cityBuild(int [][] cityArray, int dimensionX, int dimensionY){
    for(int i =0; i<dimensionY; i++){
      for(int j = 0; j<dimensionX; j++){
        cityArray[j][i] = ((int)(Math.random() * 900)+100); // generating random population for city block
      }
    }
    return;
  }
  
  public static void display(int [][] cityArray, int dimensionX, int dimensionY){
    for(int i = 0; i<dimensionY; i++){ // 0  1  2  3 .... n
      for(int j = 0; j<dimensionX; j++){
        if(cityArray[j][i]<0){
          System.out.printf("%7s", "ROBOT" ); 
        }
        else {
          System.out.printf("%7d",(int) cityArray[j][i]); 
        }
        
      // (0, 0 + j)
      }
      System.out.println("");
    }
   return;
  }
  
  
  
  public static void invade(int [][] cityArray, int robots){
    Random randomizer = new Random(); //declaring our randomizer buddy, since it's easier to do in this method
    int counter = 0;
    while (counter<robots){
      int landingSiteX = randomizer.nextInt(cityArray.length), landingSiteY = randomizer.nextInt(cityArray[1].length);
      if (cityArray[landingSiteX][landingSiteY] < 0) { 
        continue;
      }
      else{
        cityArray[landingSiteX][landingSiteY] *= -1;
      }
      counter++;
    }
    return;
  }
  
  public static void update(int[][] cityArray){
    for(int i = 0; i<(cityArray[0].length); i++){
      for(int j = 0; j<cityArray.length; j++){
        if ((cityArray[j][i])<0){ // checking for negative element of array at coordinates x, y
          if(j==0) { // if i is in the zeroth column
            cityArray[j][i]=(Math.abs(cityArray[j][i]));
          }
          else{
            cityArray[j][i]=(Math.abs(cityArray[j][i]));
            cityArray[j-1][i]=-1*cityArray[j-1][i];
          }
        }
      }
    }
    return;
  }
  
  public static void main(String [] args){
    
    int[][] cityArray; // first for row, second for column //
    int dimensionX = ((int) (Math.random()*5) + 10); //generates a random number, 10-15
    int dimensionY = ((int) (Math.random()*5) + 10); //^^
    cityArray = new int[dimensionX][dimensionY]; //decides dimensions of city using variables
    // defined above
    
    System.out.println(" ");
    System.out.println("What a lovely day in the city! Every number represents the population of that city block");
    System.out.println(" ");
    
    cityBuild(cityArray, dimensionX, dimensionY); // building city's population counts w/ method
    
    display(cityArray, dimensionX, dimensionY); // printing city's population counts w/ method
    
    
    int k = (int)((Math.random()*15)+5); //generating # of invading robots
    
    System.out.println(" ");
    System.out.println("Oh no! Robots have invaded! Blocks invaded are listed as ROBOT");
    System.out.println("However, if the humans can fight off half the robots and prevent them from reaching the capital, humanity lives on.");
    System.out.println(" ");

    invade(cityArray, k);// sending proper variables (though dimensions will not be used) to method invade()
    
    System.out.println(k +" robots have invaded! May your forces prevail.");
    System.out.println(" ");

    display(cityArray, dimensionX, dimensionY); // printing city's population counts w/ method

    for(int i = 0; i<5; i++){ //update, display in loop five times
      update(cityArray);
      System.out.println(" ");
      System.out.println("...And the robots move westward....");
      System.out.println(" ");
      display(cityArray, dimensionX, dimensionY);
    }
    
    
    //Does humanity win? Here, we will see...
    int robotRemainingCount=0;
    for(int i = 0; i<dimensionY; i++){
      for(int j = 0; j<dimensionX; j++){
        if (cityArray[j][i]<0){
          robotRemainingCount++;
        }
      }
    }
    
    if (robotRemainingCount>((k/2)+1)){
      System.out.println(" ");
      System.out.println("Humanity has lost. You are doomed!");
    }
    else{
      System.out.println(" ");
      System.out.println("Humanity has won! But the battle rages on...");
    }
    
    
    
    
    
    
  }
  
}