// Will Newbegin
//
// In which we will create a city w/ dimensions n x n
//0..........n
//.
//.
//.
//.
//.
//.
//.
//.
//.
//.
//n
import java.util.Arrays;
import java.util.Random;


public class RobotCity{
  
  public static void cityBuild(int [][] cityArray, int dimensionX, int dimensionY){
    for(int i =0; i<dimensionY; i++){
      for(int j = 0; j<dimensionX; j++){
        cityArray[j][i] = ((int)(Math.random() * 900)+100); // generating random population for city block
      }
    }
    return;
  }
  
  public static void display(int [][] cityArray, int dimensionX, int dimensionY){
    for(int i = 0; i<dimensionY; i++){ // 0  1  2  3 .... n
      for(int j = 0; j<dimensionX; j++){
        if(cityArray[j][i]<0){
          System.out.printf("%7s", "ROBOT" ); 
        }
        else {
          System.out.printf("%7d", (int) cityArray[j][i]); 
        }
        
      // (0, 0 + j)
      }
      System.out.println("");
    }
   return;
  }
  
  
  
  public static void invade(int [][] cityArray, int robots){
    Random randomizer = new Random(); //declaring our randomizer buddy, since it's easier to do in this method
    int counter = 0;
    while (counter<robots){
      int landingSiteX = randomizer.nextInt(cityArray.length), landingSiteY = randomizer.nextInt(cityArray[1].length);
      if (cityArray[landingSiteX][landingSiteY] < 0) { 
        continue;
      }
      else{
        cityArray[landingSiteX][landingSiteY] *= -1;
      }
      counter++;
    }
    return;
  }
  
  public static void update(int[][] cityArray){
    for(int i = 0; i<(cityArray[0].length); i++){
      for(int j = 0; j<cityArray.length; j++){
        if ((cityArray[j][i])<0){ // checking for negative element of array at coordinates x, y
          if(j==0) { // if i is in the zeroth column
            cityArray[j][i]=(Math.abs(cityArray[j][i]));
          }
          else{
            cityArray[j][i]=(Math.abs(cityArray[j][i]));
            cityArray[j-1][i]=-1*cityArray[j-1][i];
          }
        }
      }
    }
    return;
  }
  
  public static void main(String [] args){
    
    int[][] cityArray; // first for row, second for column //
    int dimensionX = ((int) (Math.random()*5) + 10); //generates a random number, 10-15
    int dimensionY = ((int) (Math.random()*5) + 10); //^^
    cityArray = new int[dimensionX][dimensionY]; //decides dimensions of city using variables
    // defined above
    
    System.out.println(" ");
    System.out.println("What a lovely day in the city! Every number represents the population of that city block");
    System.out.println(" ");
    
    cityBuild(cityArray, dimensionX, dimensionY); // building city's population counts w/ method
    
    display(cityArray, dimensionX, dimensionY); // printing city's population counts w/ method
    
    
    int k = (int)((Math.random()*15)+5); //generating # of invading robots
    
    System.out.println(" ");
    System.out.println("Oh no! Robots have invaded! Blocks invaded are listed as ROBOT");
    System.out.println(" ");

    invade(cityArray, k);// sending proper variables (though dimensions will not be used) to method invade()

    display(cityArray, dimensionX, dimensionY); // printing city's population counts w/ method

    for(int i = 0; i<5; i++){ //update, display in loop five times
      update(cityArray);
      System.out.println(" ");
      System.out.println("...And the robots move westward....");
      System.out.println(" ");
      display(cityArray, dimensionX, dimensionY);
    }
    
    
    
    
    
    
  }
  
}