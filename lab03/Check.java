// Will Newbegin
//  CSE 002
// Prof. Chen
// 2/9/18
// This program will obtain the original cost of a
// check from a user, along with their desired tip
// percentage, and amount of people splitting the
// check. It will then output the amount of money
// that each person must contribute to pay the 
// check.check

// importing the java scanner
import java.util.Scanner;

// setting up the program
public class Check {
// main method required for every Java program  
  public static void main(String [] args){
    
    // declaring instance of scanner
    Scanner myScanner = new Scanner ( System.in );
   
    // user input
    // prompting user for original cost of check
    System.out.print("Enter the original cost of the check in the form xx.xx: " );
    // identifying the variable obtained from the first user prompt
    double checkCost = myScanner.nextDouble();
    // prompting the user for the desired tip percentage
    System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx): " );
    // identifying the variable obtained from the second user prompt
    double tipPercent = myScanner.nextDouble();
    // converting tip percentage to a decimal
    tipPercent /= 100;
    // Prompting user for number of people splitting check
    System.out.print("Enter the number of people who went out to dinner: " );
    // identifying the variable obtained from the third user prompt
    int numPeople = myScanner.nextInt();
    
    // computation of tip and check split
    
    // assigning total cost as a double so 
    // as to maintain decimal points
    double totalCost;
    // assigning Cost/Person  as a double so 
    // as to maintain decimal points
    double costPerPerson;
    // for storing digits right of decimal pt.
    int dollars, dimes, pennies; 
    // calculating total cost 
    totalCost = checkCost * (1 + tipPercent);
    // calculating cost per person
    costPerPerson = totalCost / numPeople;
    //obtaining whole amt. without decimal fraction
    dollars=(int)costPerPerson;
    //  truncating our decimal places so we don't
    // have expenses with more than two decimals
    dimes=(int)(costPerPerson * 10) % 10;
    pennies=(int)(costPerPerson * 100) % 10;
    // outputting all our results
    System.out.println("Each person in the group owes $" + dollars + "." + dimes + pennies);

  // fin.
    
  }
}