// Will Newbegin
//  CSE 002
// Prof. Chen
// 2/11/18
// This program will obtain the volume of a regular pyramid with a square base. 
// It will prompt the user to enter the measure of the side of a square base
// as well as the pyramid's perpindicular height. It will then compute these results 
// and output them to the user.and

// Importing scanner
import java.util.Scanner;

  // program basic setup
  public class Pyramid {
    // main method for all Java programs
    public static void main(String [] args){
    
    // declarling use of a scanner for sake of recieving prompt
    Scanner myScanner = new Scanner( System.in );
      
    // obtaining user input
    // obtaining input of length of sqaure side in arbitrary units
    System.out.print("Enter the length of the square side in any preferred unit: " );
    // storing length of square side as a double
    double sqSideMeasure = myScanner.nextDouble();
    // obtaining input of pyramid height in arbitrary units
    System.out.print("Enter the height of the pyramid in the same preferred unit: " );
    // storing pyramid height measure as a double 
    double pyramidHeight = myScanner.nextDouble();
    
      
     // performing calculations
     // squaring the length of the square side to find base area
    double baseArea = Math.pow(sqSideMeasure, 2);
     // finding pyramid volume by using equation (lwh)/3
    double pyramidVolume = (baseArea * pyramidHeight)/3;
    
    // outputting results
    System.out.println("The volume of the pyramid is " +pyramidVolume + " cubic units.");
      
      
      // fin
      
     }
    
  }