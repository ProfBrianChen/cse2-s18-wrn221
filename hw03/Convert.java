// Will Newbegin
//  CSE 002
// Prof. Chen
// 2/9/18
// This program will obtain the original cost of a
// check from a user, along with their desired tip
// percentage, and amount of people splitting the
// check. It will then output the amount of money
// that each person must contribute to pay the 
// check.check

// importing the java scanner

import java.util.Scanner;

// setting up the program
public class Convert {
// main method required for every Java program  
  public static void main(String [] args){
    
    // declaring instance of scanner
    Scanner myScanner = new Scanner ( System.in );
   
    // user input
    // prompting user for affected area of hurricane, in acres
    System.out.print("Enter the affected area in acres: " );
    // identifying the variable obtained from the first user prompt
    double areaAffectedAcres = myScanner.nextDouble();
    // prompting the user for the area's rainfall in ches
    System.out.print("Enter the total rainfall in the affected area in inches: " );
    // identifying the variable obtained from the second user prompt
    double rainMeasureInches = myScanner.nextDouble();
    
    // computation of cubic miles of rain in area
   
    // Converting the the amount of rainfall to miles 
    double rainMeasureMiles = rainMeasureInches * 63360;
    // converting the acreage into square miles
    double areaAffectedSqMiles = areaAffectedAcres * 0.0015625;
    // finding the average mileage of rain per sq. mile
    // computing the volume of rainfall in the given area, in cubic miles
    double rainMeasureCubicMiles = rainMeasureMiles * areaAffectedSqMiles;
    
    
    
    // outputting our results
    System.out.println ("The total amount of rainfall, in cubic miles, is " + rainMeasureCubicMiles);
    
  
    

  // fin.
    
  }
}