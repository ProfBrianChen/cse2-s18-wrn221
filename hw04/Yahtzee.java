// Will Newbegin
//


import java.util.Scanner;


public class Yahtzee {
  
  public static void main(String [] args){
    
    Scanner myScanner = new Scanner (System.in);
    
    System.out.print("Welcome to Yahtzee! If you are ready to play, enter '1'. If not, enter '2': " );
    
    int gameReady = myScanner.nextInt();
    
    
    //Outermost if statement
    if (gameReady == 1){
      
      int die1 = (int) (Math.random () * 6 + 1);
      int die2 = (int) (Math.random () * 6 + 1);
      int die3 = (int) (Math.random () * 6 + 1);
      int die4 = (int) (Math.random () * 6 + 1);
      int die5 = (int) (Math.random () * 6 + 1);
      int aces = 0, twos =0, threes=0, fours=0, fives=0, sixes=0;
      int threeOfKind = 0, fourOfKind = 0, Yahtzee = 0;
      int upperSectionInitialTotal=0, upperSectionTotalPlusBonus=0, lowerSectionTotal=0, grandTotal=0;
    
      // checking to see if any roll categories count for bonus
      // individual die values
      // first die
      switch (die1){
       case 1:
         aces += die1;
         break;
       case 2:
         twos += die1;
         break;
       case 3:
         threes += die1;
         break;
       case 4:
         fours += die1;
         break;
       case 5:
         fives += die1;
         break;
       case 6:
         sixes += die1;
         break;
     }
      // second die
     switch (die2){
         case 1:
         aces += die2;
         break;
       case 2:
         twos += die2;
         break;
       case 3:
         threes += die2;
         break;
       case 4:
         fours += die2;
         break;
       case 5:
         fives += die2;
         break;
       case 6:
         sixes += die2;
         break;
     }
      // third die
      switch (die3){
         case 1:
         aces += die3;
         break;
       case 2:
         twos += die3;
         break;
       case 3:
         threes += die3;
         break;
       case 4:
         fours += die3;
         break;
       case 5:
         fives += die3;
         break;
       case 6:
         sixes += die3;
         break;
     }
      // fourth die
      switch (die4){
         case 1:
         aces += die4;
         break;
       case 2:
         twos += die4;
         break;
       case 3:
         threes += die4;
         break;
       case 4:
         fours += die4;
         break;
       case 5:
         fives += die4;
         break;
       case 6:
         sixes += die4;
         break;
     }
      //fifth die
      switch (die5){
         case 1:
         aces += die5;
         break;
       case 2:
         twos += die5;
         break;
       case 3:
         threes += die5;
         break;
       case 4:
         fours += die5;
         break;
       case 5:
         fives += die5;
         break;
       case 6:
         sixes += die5;
         break;
     }
      // calculating the bonuses
      upperSectionInitialTotal = die1 + die2 + die3 + die4 +die5;
      // does the +35 bonus apply? Let's check here
      if (upperSectionInitialTotal > 63){
        upperSectionTotalPlusBonus = upperSectionInitialTotal + 35;
      } 
      
      switch (aces){
        case 3:
         threeOfKind = aces;
          break;
        case 4: 
          fourOfKind = aces;
      }
        
      
      
      
      
      System.out.println("Your roll is ..." );
      System.out.println(die1 + " " +die2 + " " +die3 +" " +die4 +" " +die5);
      } // end of if that corresponds w/ user wanting to play
      
    
    // In case user doesn't want to play...
    else{
      System.out.println("Bummer! Come back another time.");
    }
    
    // fin
  }
      
  } 
