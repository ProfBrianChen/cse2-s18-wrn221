import java.util.Scanner;


public class HelloWorld{
  
  public static void linearSearch(int search, int[] array){ //method for linear search
    int wrong = 0;
    for(int i = 0; i < array.length; i++){    //runs through number of terms in the array
      if(search == array[i]){                 //if value of array is equal to search value
        System.out.println("The score "+search+" was found with "+(i+1)+" iterations."); //prints that score was found in x iterations
        wrong = 1;                            //tells program not to run next if statement
      }
    }
    if(wrong == 0){                           //if score is not found
      System.out.println("The score "+search+" was not found with 15 iterations."); //prints that score was not found
    }
  }
  public static void binarySearch(int search, int[] array){ //method for binary search
    int start = 1;                            
    int end = array.length;                   
    int count = 1;                            //initializes counter
    while (start <= end){                     //while loop
      int mid = (start + end) / 2;            //calculates middle term of the array
      if (search < array[mid]) {              //if the score is lower than the middle value of the array, lowers the search bounds 
      	end = mid - 1;                        
      }
      if (search > array[mid]) {              //if the score is greater than the middle value of the array, raises the search bounds 
        start = mid + 1;                      
      }
      if (search == array[mid]) {             //when search score is equal to array value
        System.out.println("The score "+search+" was found with "+count+" iterations."); //prints that score was found in x iterations
        break;                                //breaks while loop
      }  
      count++;                                //increments counter
    }
    if (start > end){                         //runs if binary search did not return the score
      System.out.println("The score "+search+" does not exist with "+count+" iterations."); //prints that score was not found
    }
	}
  public static void scrambleArray(int[] array){ //method to scramble arrays
    for (int i=0; i< array.length; i++) {     //for loop
      int randomizer = (int) (array.length * Math.random() ); //random term in array
      int score = array[randomizer];          //sets score equal to random term in array
      array[randomizer] = array[i];           //makes array term equal to random array term
      array[i] = score;                       //sets value to array term
    }
  }
  public static void main(String[] args){     //main method
    Scanner scanner = new Scanner(System.in); //creates scanner
    int[] studentScores = new int[15];        //creates array 'studentScores' to store 15 integers
    boolean errorCheck = true;                //creates a boolean to be turned to false if user enters errors
    int searchVal = 0;                        //creates an int to be used to search for a given value
    System.out.println("Please enter 15 ascending integers between 0-100 for the final grades in CSE2:");  //prompts user to enter 15 integers
    for (int i = 0; i < 15; i++){             //for loop that runs 15 times to assign a value to every term in array
      if(scanner.hasNextInt()){               //runs when user enters an int
        studentScores[i] = scanner.nextInt(); //sets i term in the array to the user entered int
      }
      else if (!scanner.hasNextInt()){        //runs if user does not enter an int
        System.out.println("\nERROR: Did not enter an integer\n"); //error messsage if user does not enter an int
        errorCheck = false;                   //sets errorCheck to false so that later sections of code will not run
        break;                                //breaks for loop once non-int is entered
      }
      if(studentScores[i] < 0 || studentScores[i] > 100){ //checks if value is in range 0-100
        System.out.println("\nERROR: Input not within range 0-100\n"); //error messsage if input is not in correct range
        errorCheck = false;                   //sets errorCheck to false so that later sections of code will not run
        break;                                //breaks for loop if int is not in range
      }
      if((i > 0) && (studentScores[i] <= studentScores[i-1])){ //checks if input is greater than previous input
        System.out.println("\nERROR: Input must be greater than previous value\n"); //error messsage if inputs are not in ascending order
        errorCheck = false;                   //sets errorCheck to false so that later sections of code will not run
        break;                                //breaks for loop if inputs are not ascending
      }
      scanner.nextLine();                     //clears scanner
    }
    
    if (errorCheck == true){                  //runs if no errors were made in entering array values
      System.out.println("\nCSE2 Final Scores:"); //prints header for array values
      for (int i = 0; i < 15; i++){           //for loop that runs through all values of array
        System.out.print(studentScores[i]+" "); //prints each value of the array
      }
      while (1==1){
        System.out.println("\nEnter a score to search for:"); //promps user to enter a score to search for
        if (scanner.hasNextInt()){            //checks if user enters an integer
          searchVal = scanner.nextInt();      //assigns searchVal to input value
          break;                              //breaks while loop
        }
        scanner.nextLine();                   //moves scanner to next line
      }//while
      binarySearch(searchVal, studentScores); //runs binary search on array
      scrambleArray(studentScores);           //runs method to scramble the array
      System.out.println("\nScrambled Scores:"); //prints header for array values
      for (int i = 0; i < 15; i++){           //for loop that runs through all values of array
        System.out.print(studentScores[i]+" "); //prints each value of the array
      }
      while (1==1){                           //infinite while loop
        System.out.println("\nEnter a score to search for:"); //promps user to enter a score to search for
        if (scanner.hasNextInt()){            //checks if user enters an integer
          searchVal = scanner.nextInt();      //assigns searchVal to input value
          break;                              //breaks while loop
        }
        scanner.nextLine();                   //moves scanner to next line
      }//while
      linearSearch(searchVal, studentScores); //runs linear search on scrambled array
    }//if
  }
}