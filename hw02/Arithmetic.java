//
///HW02 Arithmetic Calculations Assignment
//// Will Newbegin
//// CSE 002
//// Prof. Brian Chen
//// This program will calculate the total prices of each individual item set without tax, the tax of the item set, 
//// the total cost of the item set with tax, the cost of all the items without tax, the total tax from all three items,
//// and the total cost of each item plus tax. We will use the PA sales tax rate of 6%.

public class Arithmetic {
  public static void main(String [] args){
    //Number of pairs of pants
    int numPants = 3;
    //Cost per pair of pants
    double pantsPrice = 34.98;
    //Number of sweatshirts
    int numShirts = 2;
    //Cost per shirt
    double shirtPrice = 24.99;
    //Number of belts
    int numBelts = 1;
    //cost per box of envelopes
    double beltCost = 33.99;
    //the tax rate
    double paSalesTax = 0.06;
    
    // Calcuating the cost of pants and their tax 
    // the cost of all three pairs of pants, tax not included
    double pantsCostTotalNoTax = (numPants*pantsPrice);
    // calculating the cost of the pants' tax
    double pantsTax = (pantsCostTotalNoTax*paSalesTax);
    // calculating the total cost of the pants, with tax
    double pantsCostTotalWithTax = (pantsTax + pantsCostTotalNoTax);
    
      
    // Calculating the cost of the sweatshirts and their tax 
    // the cost of both sweatshirts, tax not included
    double shirtsCostTotalNoTax = (numShirts*shirtPrice);
 
    // calculating the cost of the sweatshirts' tax
    double shirtsTax = (shirtsCostTotalNoTax*paSalesTax);
    // calculating the total cost of the sweatshirts, with tax
    double shirtsCostTotalWithTax = (shirtsTax + shirtsCostTotalNoTax);
    
    // Calculating the cost of the belts
    // the cost of the belts, tax not included
    double beltCostTotalNoTax = (numBelts*beltCost);
    // calculating the cost of the belts' tax
    double beltTax = (beltCostTotalNoTax*paSalesTax);
    // calculating the total cost of the belts, tax and all
    double beltCostTotalWithTax = (beltCostTotalNoTax + beltTax);
      
    // Adding the three items together, no tax
    double costAllPurchasesNoTax = (beltCostTotalNoTax + 
    shirtsCostTotalNoTax + pantsCostTotalNoTax);
    // calculating the total sales tax
    double costTotalTax = (pantsTax + shirtsTax + beltTax);
    // total transaction cost calculation
    double costTotalTransactionPrice = (costTotalTax + costAllPurchasesNoTax);
    
    
   // Outputting all our final results, all nice and pretty with only two decimal places
   // The pants' price, without tax
   System.out.println("The price of all of the pants, without tax, is $" +Math.round(pantsCostTotalNoTax * 100.0)/100.0);
   // The tax of the pants 
   System.out.println("The tax total of the pants is $" +Math.round(pantsTax * 100.0)/100.00 + "0");
   // The price of the pants, tax and all
    System.out.println("The total price of the pants, tax included, is $" +Math.round(pantsCostTotalWithTax * 100.0)/100.0);
   // entering a space to break up the text
   System.out.println("  ");
    
    // The sweatshirts' price, without tax
    System.out.println("The price of all of the sweatshirts, without tax, is $" +Math.round(shirtsCostTotalNoTax * 100.0)/100.0);
   // The sweatshirts' tax
    System.out.println("The tax total of the pants is $" +Math.round(shirtsTax * 100.0)/100.00 + "0");
    // The price of the sweatshirts, tax and all
    System.out.println("The total price of the sweatshirts, tax included, is $" +Math.round(shirtsCostTotalWithTax * 100.0)/100.0);
   // entering a space to break up the text
   System.out.println("  ");
    
   // The belt's price, without tax
    System.out.println("The price of the belt, without tax, is $" +Math.round(beltCostTotalNoTax * 100.0)/100.0);
   // The belt's tax
    System.out.println("The tax total of the belt is $" +Math.round(beltTax * 100.0)/100.00);
    // The price of the belt, tax and all
    System.out.println("The total price of the belt, tax included, is $" +Math.round(beltCostTotalWithTax * 100.0)/100.0);
   // entering a space to break up the text
   System.out.println("  ");
    
    // The total calculations
    // Total price, sans tax
    System.out.println("The price of all three item types, without tax, is $" +Math.round(costAllPurchasesNoTax * 100.0)/100.0);
    // Total tax
    System.out.println("The total sales tax is $" +Math.round(costTotalTax * 100.0)/100.0);
    // total expenditure, tax and all
    System.out.println("The total transaction price is $" +Math.round(costTotalTransactionPrice * 100.0)/100.0);
      
    
    }
}