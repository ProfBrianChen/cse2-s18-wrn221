// Will Newbegin
// CSE 002
// Prof. Chen
// March 2, 2018


//  This code allows the user to input a positive integer
// greater than zero to generate a "twist" pattern that, 
// by number of columns, corresponds to the directed integer.

// importing scanner for input use
import java.util.Scanner;

// setting up format for all java files
public class TwistGenerator {
  
  public static void main(String [] args){
  
  // declaring Scanner for user input
  Scanner keyboard = new Scanner ( System. in );
    
    // soliciting integer input from user
    System.out.print("Please input a positive integer: ");
    
    //initialzing input so that it inherently creates a false statement
    int limit = -1;
    
    // test condition to loop while user is not inputting a positive integer
    Boolean correctVal = true;
    
    // testing if user input a proper integer
    if (keyboard.hasNextInt()){
      limit = keyboard.nextInt();
    }
    else {
      keyboard.nextLine();
    }
    if (limit < 1){
      correctVal = false;
    }
      
      // testing boolean variable to see if the program must again solicit proper input
      while (!correctVal){
        System.out.print("Please input a positive integer: ");
        if (keyboard.hasNextInt()){
          limit = keyboard.nextInt();
        }
        else {
          keyboard.nextLine();
        }
        if (limit > 0){
          correctVal = true;
        }
      }
    
        
        // first row output
        for (int i = limit; i >= 1; i--){
          if((limit-i) % 3 == 1){
            System.out.print(" ");
            }
          if((limit-i) % 3 == 2){
            System.out.print("/"); 
            }
          if((limit-i) % 3 == 0){
            System.out.print("\\");
            }  
        }
        //second row output
        System.out.println("");
        for (int i = limit; i >= 1; i--){
          if((limit-i) % 3 == 1){
            System.out.print("X");
            }
          if((limit-i) % 3 == 2){
            System.out.print(" "); 
            }
          if((limit-i) % 3 == 0){
            System.out.print(" ");
            }  
        }
      // third row output
    System.out.println(" ");
    for (int i = limit; i >= 1; i--){
          if((limit-i) % 3 == 1){
            System.out.print(" ");
            }
          if((limit-i) % 3 == 2){
            System.out.print("\\"); 
            }
          if((limit-i) % 3 == 0){
            System.out.print("/");
            }  
        }
   System.out.println();     
        
      
    }
   
    
    
    
  }
