// Will Newbegin
// Feb. 2, 2018 
// CSE 002: Foundations of Programming
// This program, called Cyclometer, 
// will output a given bike trip's minutes, 
// front wheel rotation count, distance, and 
// total distance between two trips.

public class Cyclometer {
    	// main method required for every Java program
   	public static void main(String[] args) {

      // our input data:
      int secsTrip1=480; // how much time our first trip took
      int secsTrip2=3220; // how much time our first trip took
      int countsTrip1=1561; // how many rotations the front wheel made on our first trip
      int countsTrip2=9037; // how many rotations the front wheel made on our second trip
      double wheelDiameter=27.0; // the precise diameter of our front bike wheel
      double PI=3.14159; // A conveniently abbreviated representation of a conveneint circumferential constant
      int feetPerMile=5280; // allowing the computer to understand how many feet constittute one mile
      int inchesPerFoot=12; // allowing the computer to understand how many inches constittute one foot
      int secondsPerMinute=60; // allowing the computer to understand how many seconds constittute one minute
      double distanceTrip1, distanceTrip2, totalDistance; // distingusihting the distances taken on each trip and their respective summation as a double variable
     
      // outputting results, part 1.
      System.out.println ("Trip 1 took " +(secsTrip1/secondsPerMinute)+" minutes and had "+countsTrip1 +" counts."); // the output of time and wheel rotation from our first trip.
      System.out.println ("Trip 2 took " +(secsTrip2/secondsPerMinute)+" minutes and had "+countsTrip2 +" counts."); // the output of time and wheel rotation from our second trip.
      
      // computing distances of our trips in inches with previously defined variables
      distanceTrip1=countsTrip1*wheelDiameter*PI; // identifying the distance 
      // of our first trip in inches by calculating circumference of a circle
      // with diameter *  PI and equivalating that to a prescribed distance traveled
	    distanceTrip1/=inchesPerFoot*feetPerMile; // convertining our distnace into miles
	    distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile; // more
      //of the same; we are calculating,using similar methods to the first trip, 
      // the distance traveled on our second trip. 
    	totalDistance=distanceTrip1+distanceTrip2; // here, we calculate the total 
      // distance traveled by adding the distances of our two trips together.

      //outputting results, part 2.
      System.out.println("Trip 1 was "+distanceTrip1+" miles"); 
      // outputting the distance of the first trip 
	    System.out.println("Trip 2 was "+distanceTrip2+" miles"); 
      // outputting the distance of the second trip
    	System.out.println("The total distance was "+totalDistance+" miles");
      // outputting the total distance of the trip

	}  //end of main method   
} //end of class
