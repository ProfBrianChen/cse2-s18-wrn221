// Will Newbegin

import java.util.Random; // importing our Random buddy
import java.util.Scanner; // importing our Scanner buddy

public class StoryGenerator{ // main class
  
  
  
  public static String adj(){
    String adjChoice = " ";
    int wordChoice = (int) (Math.random() * 9 + 1);
    switch (wordChoice){
      case 1: adjChoice = "sloppy";
        break;
      case 2: adjChoice = "unilateral";
        break;
      case 3: adjChoice = "brown";
        break;
      case 4: adjChoice = "crispy";
        break;
      case 5: adjChoice = "disgruntled";
        break;
      case 6: adjChoice = "unfortunate";
        break;
      case 7: adjChoice = "throbbing";
        break;
      case 8: adjChoice = "machiavellian";
        break;
      case 9: adjChoice = "intolerant";
        break;
    }
    return adjChoice;
  }
  public static String nounSub(){ //subject
    String nounChoice = " ";
    int wordChoice = (int) (Math.random() * 9 + 1);
    switch (wordChoice){
      case 1: nounChoice = "milk";
        break;
      case 2: nounChoice = "hole";
        break;
      case 3: nounChoice = "blade";
        break;
      case 4: nounChoice = "camel";
        break;
      case 5: nounChoice = "karate";
        break;
      case 6: nounChoice = "science";
        break;
      case 7: nounChoice = "cusp";
        break;
      case 8: nounChoice = "equilibrium";
        break;
      case 9: nounChoice = "caribou";
        break;
    }
    return nounChoice;
  }
  public static String verbs(){ // verb
    String verbChoice = " ";
    int wordChoice = (int) (Math.random() * 9 + 1);
    switch (wordChoice){
      case 1: verbChoice = "fought";
        break;
      case 2: verbChoice = "greased";
        break;
      case 3: verbChoice = "crushed";
        break;
      case 4: verbChoice = "taught";
        break;
      case 5: verbChoice = "bodyguarded";
        break;
      case 6: verbChoice = "disregarded";
        break;
      case 7: verbChoice = "helped";
        break;
      case 8: verbChoice = "pushed";
        break;
      case 9: verbChoice = "munched";
        break;
    }
    return verbChoice;
  }
  public static String verbs2(){ // second set of verbs
    String verbChoice = " ";
    int wordChoice = (int) (Math.random() * 9 + 1);
    switch (wordChoice){
      case 1: verbChoice = "misunderstood";
        break;
      case 2: verbChoice = "betrayed";
        break;
      case 3: verbChoice = "stumped";
        break;
      case 4: verbChoice = "flicked";
        break;
      case 5: verbChoice = "shunned";
        break;
      case 6: verbChoice = "exorcized";
        break;
      case 7: verbChoice = "delivered";
        break;
      case 8: verbChoice = "studied";
        break;
      case 9: verbChoice = "impersonated";
        break;
    }
    return verbChoice;
  }
  public static String nounObjSing(){ // object of sentence
    String nounChoice = " ";
    int wordChoice = (int) (Math.random() * 9 + 1);
    switch (wordChoice){
      case 1: nounChoice = "half-hearted performance";
        break;
      case 2: nounChoice = "cult of personality";
        break;
      case 3: nounChoice = "vegan diet";
        break;
      case 4: nounChoice = "gestation period";
        break;
      case 5: nounChoice = "crevice";
        break;
      case 6: nounChoice = "caribou";
        break;
      case 7: nounChoice = "bunsen burner";
        break;
      case 8: nounChoice = "ship of Captain Blackbeard";
        break;
      case 9: nounChoice = "litterbin";
        break;
    }
    return nounChoice;
  }
  public static String nounObjPlur(){ // object of sentence
    String nounChoice = " ";
    int wordChoice = (int) (Math.random() * 9 + 1);
    switch (wordChoice){
      case 1: nounChoice = "children of Tom Cruise";
        break;
      case 2: nounChoice = "cardiovascular issues";
        break;
      case 3: nounChoice = "vikings";
        break;
      case 4: nounChoice = "housing crises";
        break;
      case 5: nounChoice = "Rastafarians";
        break;
      case 6: nounChoice = "well-regulated militia";
        break;
      case 7: nounChoice = "washed-up child actors";
        break;
      case 8: nounChoice = "shark attacks";
        break;
      case 9: nounChoice = "failures of the milennial generation";
        break;
    }
    return nounChoice;
  }
  
  public static void structure1(String subject){
    System.out.println("This particular " + subject + ", " + adj() + " in nature, " + verbs() + " " + nounObjPlur() + ".");
    return;
    
  }
  
  public static void structure2(String subject){
    System.out.println("Our " + subject + " was known as a " + adj() + " " + nounObjSing() + ".");
    return;
  }
  
  public static void structure3(String subject){
    System.out.println("It " + verbs2() + " and " + verbs() + " 'till the day was done. ");
    return;
  }
  
  public static void structure4(String subject){
    System.out.println("Many " + nounObjPlur() + " and their " + nounObjPlur() + " " + verbs2() + " this " + subject + " with a " + nounObjSing() + ".");
    return;
  }
  
  public static void StoryRun (int sentCount){ // will compile story body
    
    String sentenceSubject = nounSub();
    int sentenceStructureCount = 0;
    System.out.println("Once, there was a " + sentenceSubject + ".");
    System.out.println("");
    for (int i = 0; i <= sentCount; i++){
      sentenceStructureCount = (int) (Math.random() * 4 +1);
      switch (sentenceStructureCount){
        case 1: structure1(sentenceSubject);
          break;
        case 2: structure2(sentenceSubject);
          break;
        case 3: structure3(sentenceSubject);
          break;
        case 4: structure4(sentenceSubject);
          break;
      }
    }
    System.out.println("");
    System.out.println("...And that is the tale of the " + adj() + " " + sentenceSubject + ".");
  }
  
  
  public static void main (String [] args){ // main method
    Scanner keyboard = new Scanner (System.in);
    boolean inputCheck = true;
    int sentCount = 0;
    
    System.out.print("Welcome to Story Generator! How many sentences would you like to print? Enter a positive integer: ");
    if (keyboard.hasNextInt()){
      sentCount = keyboard.nextInt();
    }
    else {
      keyboard.nextLine();
    }
    if (sentCount < 1){
      inputCheck = false;
    }
    
    
    while (inputCheck == false){
      System.out.print("Please input a positive integer for the amount of sentences you would like to print: ");
      if (keyboard.hasNextInt()){
      sentCount = keyboard.nextInt();
      }
      else {
        keyboard.nextLine();
      }
      if (sentCount < 1){
        inputCheck = false;
      }
      else {
        inputCheck = true;
      }
     
      
    }
    StoryRun(sentCount);
  }
    
    
}