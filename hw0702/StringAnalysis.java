// Will Newbegin
// CSE 002
// Prof. Chen
// 3/27/18


// importing our scanner buddy 
import java.util.Scanner;

// this program will process string by examining all characters
// (or as many as instructed) in a given string variable, determining
// if they are letters or not.

// main class
public class StringAnalysis {
  
  // check method 1
  public static Boolean check(String letter){
    int length = letter.length();
      for (int i=0; i<=length; i++){
        if (Character.isLetter(letter.charAt(i))){
          continue;
        }
        else{
          return false;
        }
      }
      return true;
   }
    
  
  // check method 2
  public static Boolean check(String letter, int charCheck){
    int stringLength = letter.length();
    if (charCheck > stringLength){
     return false;
    }
    else if (charCheck <= 1){
      return false;
    }
    else {
      for (int i=0; i<=charCheck; i++){
        if (Character.isLetter(letter.charAt(i))){
          continue;
        }
        else{
          return false;
        }
      }
      return true;
    }
  }
  
  
  // main method
  public static void main (String [] args){
    
    // declaring Scanner
    Scanner keyboard = new Scanner (System.in);
    
    // declaring, initializing variables
    String varStr = " ", countX = " ", yesNo = " ";
    int letterCount = 0;
    Boolean allLetters = true;
    
    
    // soliciting user input, string
    System.out.println("This program will analyze a string variable to see if it is comprised of all letters.");
    System.out.print("Enter the string variable that you would like to check: ");
    
    // checking to see if the user did, in fact, input string
    if (keyboard.hasNext()){
      varStr = keyboard.next();
    }
    else {
      keyboard.nextLine();
    }
    
    // specific # of characters for checking?
    System.out.print("Do you wish to check for a specific number of characters? If so, enter 'yes', and if not, enter 'no' : ");
    if (keyboard.hasNext()){
      yesNo = keyboard.next();
    }
    // if user says yes:
    if (yesNo.equals("yes")){
      System.out.println("You will now enter the amount of characters in the string that you want to check.");
      System.out.print("Enter the amount of charcters here: ");
      if (keyboard.hasNextInt()){ //check
        letterCount = keyboard.nextInt();
       }
       else {
        keyboard.nextLine();
       }
      // calling proper method, method 2
      allLetters = (Boolean) check(varStr, letterCount);
      // System output
      if ((allLetters == false)){
        System.out.println("The string that you input was " + varStr + ". However, either one or more of the characters that you input was not a letter");
        System.out.print("OR your check-to length, " + letterCount + ", was longer than the string variable.");
      }
      if (allLetters == true){
        System.out.print("The string that you input,  " + varStr + ", consisted of all letters up through " + letterCount + " characters.");
      }
    }
    // if user says no
    else if (yesNo.equals("no")){
      // calling proper method, method 1
      allLetters = (Boolean) check(varStr);
      // System output
      if ((allLetters == false)){
        System.out.println("The string that you input was " + varStr + ". However, one or more of the characters that you input was not a letter.");
      }
      if (allLetters == true){
        System.out.print("The string that you input was " + varStr + ". All of the characters were letters. ");
      }
    }
    
    
    
    
    
  }
}