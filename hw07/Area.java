// Will Newbegin
// CSE 002
// Prof. Chen
// 3/27/18


// importing our scanner buddy 
import java.util.Scanner;

public class Area{
  
  
  
  // circle area function method
  public static double areaCircle(double radius){
    return (Math.pow(radius, 2)) * 3.14;
  }
  
  // rectangle area function method
  public static double areaRectangle(double width, double height){
    return width*height;
  }
  
  // triangle area function method
  public static double areaTriangle(double base, double height){
    return (base/2) * height;
  }
  
  // main method
  public static void main (String [] args){
    
    
    //declaring Scanner
    Scanner keyboard = new Scanner (System.in);
    
    //declaring, intializng other variables
    String shapeType = " ";
    Boolean properInputString = true, properInputInt = true;
    double pi = 3.14;
    double width = 0, base = 0, heightTriangle = 0, radius = 0, heightRectangle = 0;
    double areaTriangleResult = 0, areaRectangleResult = 0, areaCircleResult = 0;
    
    
      
    
    /////////
    // soliciting user input
    System.out.print("Enter the shape whose area you would like to find; triangle, rectangle, or circle: ");
    
     if (keyboard.hasNext()) {
       shapeType = keyboard.next();
     }
    else {
      keyboard.nextLine();
    }
    if (shapeType.equals("triangle")){
      System.out.print("Enter the base measurement of your triangle: ");
        if (keyboard.hasNextInt()){
          properInputInt = true;
          base = keyboard.nextInt();
        }
        else {
          keyboard.nextLine();
        }
        // making sure base input is within proper parameters
        if (base > 0){
          properInputInt = true;
        }
        else {
          properInputInt = false;
        }
        
        // re-asking for base measurement
        while (!properInputInt){
          System.out.print("Enter the base measurement of the triangle: ");
          if (keyboard.hasNextInt()){
            properInputInt = true;
            base = keyboard.nextInt();
          }
          else {
            keyboard.nextLine();
          }
          if (base > 0){
            properInputInt = true;
          }
          else {
            properInputInt = false;
          }
        }
      
      // asking for triangle height 
      System.out.print("Enter the height of your triangle: ");
        if (keyboard.hasNextInt()){
          properInputInt = true;
          heightTriangle = keyboard.nextInt();
        }
        else {
          keyboard.nextLine();
        }
        // making sure base input is within proper parameters
        if (heightTriangle > 0){
          properInputInt = true;
        }
        else {
          properInputInt = false;
        }
        
        // re-asking for height measurement
        while (!properInputInt){
          System.out.print("Enter the height measurement of the triangle: ");
          if (keyboard.hasNextInt()){
            properInputInt = true;
            heightTriangle = keyboard.nextInt();
          }
          else {
            keyboard.nextLine();
          }
          if (heightTriangle > 0){
            properInputInt = true;
          }
          else {
            properInputInt = false;
          }
        }
      
      // calling the triangle area method
      areaTriangleResult = (double) areaTriangle(base, heightTriangle);
      // outputting results
      System.out.println("The area of your triangle is " + areaTriangleResult + " square units.");
    }
    
    
    /////////
    // working with rectangle input
    else if (shapeType.equals("rectangle")){
      
      //solicitng rectangle width dimensions
      System.out.print("Enter the width measurement of your rectangle: ");
        if (keyboard.hasNextInt()){
          properInputInt = true;
          width = keyboard.nextInt();
        }
        else {
          keyboard.nextLine();
        }
        // making sure width input is within proper parameters
        if (width > 0){
          properInputInt = true;
        }
        else {
          properInputInt = false;
        }
        
        // re-asking for width measurement
        while (!properInputInt){
          System.out.print("Enter the width measurement of the rectangle: ");
          if (keyboard.hasNextInt()){
            properInputInt = true;
            width = keyboard.nextInt();
          }
          else {
            keyboard.nextLine();
          }
          if (width > 0){
            properInputInt = true;
          }
          else {
            properInputInt = false;
          }
        }
      
      // asking for height dimensions of the rectangle
      System.out.print("Enter the height of your rectangle: ");
        if (keyboard.hasNextInt()){
          properInputInt = true;
          heightRectangle = keyboard.nextInt();
        }
        else {
          keyboard.nextLine();
        }
        // making sure rectangle height input is within proper parameters
        if (heightRectangle > 0){
          properInputInt = true;
        }
        else {
          properInputInt = false;
        }
        
        // re-asking for height measurement, checking for proper input
        while (!properInputInt){
          System.out.print("Enter the height measurement of the rectangle: ");
          if (keyboard.hasNextInt()){
            properInputInt = true;
            heightRectangle = keyboard.nextInt();
          }
          else {
            keyboard.nextLine();
          }
          // making sure rectangle height input is within proper parameters 
          if (heightRectangle > 0){
            properInputInt = true;
          }
          else {
            properInputInt = false;
          }
        }
      
      //calling rectangle area method
      areaRectangleResult = (double) areaRectangle(heightRectangle, width);
      
      // outputting results
      System.out.print("Your rectangle has an area of " + areaRectangleResult + " square units. "); 
    }
    
    
    
    
    
    /////////
    // working with a circle 
    else if (shapeType.equals("circle")){
      System.out.print("Enter the radius measurement of your circle: ");
        if (keyboard.hasNextInt()){
          properInputInt = true;
          radius = keyboard.nextInt();
        }
        else {
          keyboard.nextLine();
        }
        // making sure width input is within proper parameters
        if (radius > 0){
          properInputInt = true;
        }
        else {
          properInputInt = false;
        }
        
        // re-asking for width measurement
        while (!properInputInt){
          System.out.print("Enter the radius measurement of the circle: ");
          if (keyboard.hasNextInt()){
            properInputInt = true;
            radius = keyboard.nextInt();
          }
          else {
            keyboard.nextLine();
          }
          if (radius > 0){
            properInputInt = true;
          }
          else {
            properInputInt = false;
          }
        }
      
      //calling rectangle area method
      areaCircleResult = (double) areaCircle(radius);
      
      // outputting results
      System.out.print("Your circle has an area of " + areaCircleResult + " square units"); 
    }
    else {
      System.out.print("You didn't provide proper input. The program will now end. ");
        
    }
  }
}