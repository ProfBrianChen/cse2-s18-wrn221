Grade: 88.
-5 (program 1- Area) Program terminates when user enters invalid input for shape choice (loop should continue to run until valid input is provided). 
-4 (program 1- Area) missing separate method for accepting user input and checking its validity. 
-3 (program 2- StringAnalysis) program terminates if user enters invalid input when asked to select to have all characters evaluated or only a certain quantity (rather than continuation of loop).
Work on checking, ensuring validity of user input using loops. 
Also, in the future, if a homework assignment requires 2 separate programs, organize both programs into only one homework folder (a single folder can hold multiple files). 
